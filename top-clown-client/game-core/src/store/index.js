import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    ws: null,
    rounds: {},
    score: 0,
    room: {},
    token: ''
  },
  mutations: {
    enterRoom: ((state, payload) => {
      state.room = payload;
    }),
    addRound: ((state, payload) => {
      state.rounds[payload.num] = payload.phrases
    }),
    addScore: ((state, payload) => {
      state.score += payload;
    }),
    setToken: (state, payload) => {
      state.token = payload
    },
    setWS: ((state, payload) => {
      state.ws = payload
    })
  },
  actions: {
  },
  getters: {
    getWS: state => state.ws,
    getRoom: state => state.room,
    getScore: state => state.score,
    getToken: state => state.token
  }
})
