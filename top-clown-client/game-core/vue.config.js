const path = require('path');

module.exports = {
    "publicPath": "./",
    "outputDir": path.resolve(path.dirname(__dirname), "dist"),
    "assetsDir": "assets",
    "transpileDependencies": [
        "vuetify"
    ],
}