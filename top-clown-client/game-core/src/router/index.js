import Vue from 'vue'
import VueRouter from 'vue-router'
import StartScreen from "../views/StartScreen";
import PlayableArea from "../views/PlayableArea";


Vue.use(VueRouter)

  const routes = [
    {
      name: 'start',
      path: '/start',
      component: StartScreen
    },
    {
      name: 'game',
      path: '/game',
      component: PlayableArea
    }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
